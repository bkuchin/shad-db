#include <cassert>
#include <random>

#include "discovery.h"

Discovery::Discovery(ActorId id)
    : ids_{id}, key_intervals_{{KEY_LOWER_BOUND, KEY_UPPER_BOUND}} {}

Discovery::Discovery(std::vector<ActorId> ids,
                     std::vector<KeyInterval> key_intervals)
    : ids_(std::move(ids)), key_intervals_(std::move(key_intervals)) {
  assert(ids_.size() == key_intervals_.size());
  assert(ids_.size() > 0);
}

ActorId Discovery::get_by_key(Key key) const {
  ActorId id = UNDEFINED_ACTOR_ID;
  for (size_t i = 0; i < key_intervals_.size(); ++i) {
    auto interval = key_intervals_[i];
    if (key >= interval.begin && key < interval.end) {
      assert(id == UNDEFINED_ACTOR_ID);
      id = ids_[i];
    }
  }
  assert(id != UNDEFINED_ACTOR_ID);
  return id;
}

ActorId Discovery::get_random() const {
  static std::mt19937 generator(42);
  return ids_[std::uniform_int_distribution<int>(0,
                                                 ids_.size() - 1)(generator)];
}
