#pragma once

#include <cstdint>
#include <limits>

// ID actor. All actors in the system - servers and clients - have unique IDs.
using ActorId = int;

// All events in the system have unique timestamps
using Timestamp = int64_t;

// Unique ID of transaction.
using TransactionId = int64_t;

// Unique ID of every passed message.
using RequestId = int64_t;

// Key/Value types, for reading and writing.
using Key = int64_t;
using Value = int64_t;

constexpr Timestamp UNDEFINED_TIMESTAMP = -1;
constexpr TransactionId UNDEFINED_TRANSACTION_ID = -1;
constexpr ActorId UNDEFINED_ACTOR_ID = -1;
constexpr Value DEFAULT_VALUE = 0;

constexpr Key KEY_LOWER_BOUND = std::numeric_limits<Key>::min();
constexpr Key KEY_UPPER_BOUND = std::numeric_limits<Key>::max();
