#pragma once

#include "accessors.h"
#include "ast.h"
#include "jit.h"

namespace shdb {

std::optional<Type>
    infer_type(std::shared_ptr<Ast> ast, const std::shared_ptr<SchemaAccessor> &schema_accessor = nullptr);

JitValue generate(
    Jit &jit,
    std::shared_ptr<Ast> expression,
    const JitRow &jitrow = {},
    const std::shared_ptr<SchemaAccessor> &schema_accessor = nullptr);

}    // namespace shdb
