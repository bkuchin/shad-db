#!/bin/bash

user_commit=(
  "gepardo" "9994680b7f86c0d642cc640c978ac5eb2218fc1b"
  "bkuchin" "9aa58d1688113430af8ad13f35da350380650dfa"
  # Last before deadline, 
  "erzyadev" "c3ce4e3812a02d3e623624d11e10bb1f4918422e"
  "kononovk" "ba70e0ab6b9fcf8a4ee09319de5bae2b8f26b392"
  "zeronsix" "4cd77642a218ac70abf449063ef44333f4ac45ab"
  "nsvasilev" "1f6545a4531867709a3335a0624e2a6c70690647"
  "faucct" "435c679c6c03dc86d931d38aeccf2a27836668c9"
  "svlads" "d38d62b8bb365e7ba967bd1375f54d4277cc2930"
  "d4rk" "2aa4cae993e42c1a8016f25ddaa6dbb327fe2b51"
  "kitaisreal" "d1a2cbea6c3faa2cc65e93582c050385b2e192eb"
  "Fedya001" "42abe1f569b1e882165c7bc260780b266e5ce5c8"
  "AlexJokel" "d81a473cfe33d007aa52886ee8a78a6cd2932117"
  "yesenarman" "411687b1c126f1654317d35b4c02da766ead097e"
  "gripon" "dfae475a52ffbf6be532c4c8a931148296a4fada"
  "Kifye" "dfae475a52ffbf6be532c4c8a931148296a4fada"
  "dkhorkin" "e5aa791133fc3613981572b551a542a17bd3f2b4"
  "mbkkt" "312e6447c04ef834aeddd24650965aeedb814863"
  "pervakovg" "eb4d199cd87bbcfd9e5bee5ea02c59c921691a9d"
  "Panesher" "388a2ba6b0546f1f5e5dbe9125007de305e146e9"
  "TEduard" "a5c49e7dbd3417c06ccadbb174e172a94f6da530"
  "alexeysm" "caeac09e127877e2a92525aa2a2bbbe949989906"
  "bazarinm" "5e9fef514363ae2945d70f39414470df4aa711d9"

  # Reference implementation
  "dnorlov" "6f925693331fb005ba8068e5bf533552af304653"
)

len=${#user_commit[@]}
let limit=len/2-1

for i in `seq 0 $limit`
do
  let u=2*i
  let v=2*i+1
  username=${user_commit[$u]}
  commit=${user_commit[$v]}
  cd $username
  git checkout $commit
  cd ..
done
