#!/usr/bin/python

import sys

blacklist = (
  ("Kifye", 2),
  ("Kifye", 3),
  
  ("alexeysm", 2),
  ("alexeysm", 3),
  ("alexeysm", 4),
  
  ("bazarinm", 3),
  ("bazarinm", 4),
  ("bazarinm", 5),
  ("bazarinm", 6),
  
  ("dkhorkin", ),
  ("gripon", ),
  ("pervakovg", ),
  ("Panesher", ),
)

if (sys.argv[1], int(sys.argv[2])) in blacklist or (sys.argv[1], ) in blacklist:
  sys.exit(0)

sys.exit(1)
